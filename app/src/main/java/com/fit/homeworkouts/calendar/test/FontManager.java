package com.fit.homeworkouts.calendar.test;

import android.content.Context;
import android.graphics.Typeface;

public class FontManager {

    private static final boolean IS_FONT_ENABLED = true;
    private static Typeface regular;
    private static Typeface bold;

    public static FontManager getInstance() {
        return Holder.INSTANCE;
    }

    public static void configure(Context context) {
        if (IS_FONT_ENABLED) {
            regular = Typeface.createFromAsset(context.getAssets(), "font/Roboto/Roboto-Light.ttf");
            bold = Typeface.createFromAsset(context.getAssets(), "font/Roboto/Roboto-Regular.ttf");
        }
    }

    public static Typeface getRegular() {
        return regular;
    }

    public static Typeface getBold() {
        return bold;
    }

    private static final class Holder {
        private static final FontManager INSTANCE = new FontManager();
    }
}